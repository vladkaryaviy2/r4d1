
<!-- Main R4D Video Configuration -->

<div class="embed-responsive embed-responsive-16by9" id="main_video_setup_error" style="display: none;">
    <img src="{{asset('error.jpg')}}" class="error-image" alt="{{Setting::get('site_name')}} - Main Video">
</div>

<div class="embed-responsive embed-responsive-16by9" id="main_video_ad" style="display: none">
    <img src="" class="error-image" alt="{{Setting::get('site_name')}} - Main Video Ad" id="ad_image">

    <div class="click_here_ad" style="display: none">

    </div>

    <div class="ad_progress">
   		<div id="timings">{{tr('ad') }} : <span class="seconds"></span></div>
    	<div class="clearfix"></div>
	    <div id="progress">
        
      </div>
	</div>
</div>
@if(count($videos_list) > 0)
@foreach($videos_list as $video_item)
    <?php
    $video_format = 'mp4';
    if($video_item && isset($video_item->video)) {
        $temp = explode('.', $video_item->video);
        $video_format = end($temp);
    }
    ?>
    <video
        id="stream_video_{{$video_item['id']}}"
        class="video-js col-md-6 vjs-big-play-centered vjs-layout-medium vjs-default-skin hidden"
        controls
        preload="auto"
        poster="{{$video->default_image}}"
        data-setup= '{ "playbackRates": [0.5, 1, 1.5, 2] }'
      >
        <source src="{{$video_item['video']}}" type="video/{{$video_format}}" />
        <p class="vjs-no-js">
          <!-- To view this video please enable JavaScript, and consider upgrading to a
          web browser that
          <a href="https://videojs.com/html5-video-support/" target="_blank"
            >supports HTML5 video</a> -->
        </p>
      </video>
@endforeach
@endif
      <div id="video_player_overlay">
        <img src="{{Setting::get('site_logo')}}" class="overlay_logo" />
      </div>
 <div class="embed-responsive embed-responsive-16by9" id="flash_error_display" style="display: none;">
         <div style="width: 100%;background: black; color:#fff;height: 100%;">
               <div style="text-align: center;align-items: center;">{{tr('flash_missing')}}<a target="_blank" href="http://get.adobe.com/flashplayer/" class="underline">{{tr('adobe')}}</a>.</div>
         </div>
  </div>


@if(!check_valid_url($video->video))
    <div class="embed-responsive embed-responsive-16by9" style="display:none" id="main_video_error">
        <img src="{{asset('error.jpg')}}" class="error-image" alt="{{Setting::get('site_name')}} - Main Video">
    </div>
@endif


<div class="embed-responsive embed-responsive-16by9" id="flash_error_display" style="display: none;">
   <div style="width: 100%;background: black; color:#fff;height:350px;">

   		 <div style="text-align: center;padding-top:25%">{{tr('flash_missing')}}<a target="_blank" href="http://get.adobe.com/flashplayer/" class="underline">{{tr('adobe')}}</a>.</div>
   </div>
</div>

@section('r4d_video_script')
<script>
@if(count($videos_list) > 0)
var video_id = []
var player_order = 0
@foreach($videos_list as $video_item)
player_order++;
var options_{{$video_item['id']}} = {
    domid: player_order,
    responsive: true,
    breakpoints: {
        tiny: 300,
        xsmall: 400,
        small: 500,
        medium: 600,
        large: 700,
        xlarge: 800,
        huge: 900
    }
};
var player_{{$video_item['id']}} = videojs("stream_video_{{$video_item['id']}}", options_{{$video_item["id"]}}, function(){
    this.on('ended', function() {
        this.addClass('vjs-ended');
        if (this.options_.loop) {
            this.currentTime(0);
            this.play();
        } else if (!this.paused()) {
            this.pause();
        }
        videojs.log('ed errer!');
        // this.trigger('ended');
        this.addClass('hidden');
        if(video_id.length != options_{{$video_item['id']}}.domid) {
            play_next('end',  options_{{$video_item['id']}}.domid)
        }else {
            play_next('end',  0)
        }
   })
   this.on('error', function() {
        this.addClass('vjs-error');
        videojs.log('playback errer!', options_{{$video_item['id']}}.domid);
        this.addClass('hidden');
        if(video_id.length != options_{{$video_item['id']}}.domid) {
            play_next('end',  options_{{$video_item['id']}}.domid)
        }else {
            play_next('end',  0)
        }
   })
  this.on('playing', function() {
      videojs.log('playback began!', options_{{$video_item['id']}}.domid)
  });
})
// player_{{$video_item['id']}}.logo({
//     image:'my_logo.png',
//     position:'top-left'
// })
video_id.push({{$video_item['id']}})
@endforeach
@endif
console.log(video_id,"video_id")
$('#stream_video_'+video_id[0]).removeClass('hidden')
function play_next(status, domid) {
    $('#stream_video_'+video_id[domid]).removeClass('hidden')
    eval('player_'+video_id[domid]).play()
    // if(status == 'end' && domid == 0) {
    //     $('#stream_video_'+video_id[0]).removeClass('hidden')
    //     eval('player_'+video_id[0]).play()
    // }else {
    //     $('#stream_video_'+video_id[domid]).removeClass('hidden')
    //     eval('player_'+video_id[domid]).play()
    // }
    // if(video_id.length == domid) {

    // }
    
}
</script>
@endsection
<!-- Trailer Video Configuration END -->